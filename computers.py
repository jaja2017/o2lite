#!/usr/bin/env python3
""" Test eines Oracle to sqlite3 Treibers """
import cx_Oracle

def ausgabe(result):
    rows = result.fetchall()
    for row in rows:
        print(row)
    return True

# vollkommen ueberfluessige parameter
user = "nutzer"
pw = "passwort"
dsn = "https://rpf-futurelearn.s3-eu-west-1.amazonaws.com/Programming+103+(Data)/computer_cards.db"
# 
dsnstring= cx_Oracle.makedsn(user, pw, dsn)
# conn = cx_Oracle.connect(user, pw, dsn)
conn = cx_Oracle.connect(dsnstring)

print("1. Abfrage mit conn.execute")
query= "SELECT * FROM computer WHERE ram >= 1024"
print("conn.execute({})".format(query))
result1 = conn.execute(query)
ausgabe(result1)

print()

print("2. Abfrage ueber Cursor Objekt")
cur = conn.cursor()
print("conn.execute({})".format(query))
cur.execute(query)
ausgabe(cur)
cur.close()

print()

print("3. cur.fetchone funktioniert noch nur bei Abfrageuebergabe")
cur = conn.cursor()
cur.execute(query)
row = cur.fetchone()
print(row)
row = cur.fetchone()
print(row)
cur.close()

print()

print("4. cur.fetchmanny funktioniert noch nur bei Abfrageuebergabe")
print("der **Parameter numRows = 3 wird nicht unterstuetzt")
cur = conn.cursor()
cur.execute(query)
rows = cur.fetchmany(3)
print(rows[2])
cur.close()

print()

print("5. Abfrage mit cur.prepare und cur.execute")
query= "SELECT * FROM computer WHERE ram >= :1 AND ram <= :2"
parameter= (1024,1024)
cur= conn.cursor()
print(type(cur))
print("cur.prepare({})".format(query))
cur.prepare(query)
print("cur.execute({})".format(parameter))
result2 = cur.execute(parameter)
ausgabe(result2)

print()

print("6. Abfrage ueber cur.execute und Parametern")
query= "SELECT * FROM computer WHERE ram >= :1 AND ram <= :2"
parameter= (2024,2048)
print("cur.execute({},{})".format(query,parameter))
cur= conn.cursor()
cur.execute(query,parameter)
res2b = cur.fetchall()
print(res2b)
cur.close()

print()

print("8. binding data an **Symbole mit cur.prepare")
query= "SELECT * FROM computer WHERE ram = :size"
print("cur.execute( size = 2048)")
cur= conn.cursor()
cur.prepare(query)
cur.execute(size = 2048)
res2c = cur.fetchall()
print(res2c)
cur.close()

print()


conn.close()


