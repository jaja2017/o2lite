#!/usr/bin/env python3
'''UPDATE FROM wird erst ab sqlite3 3.33.0 unterstuetzt,
welches mit dem aktuellen manjaro,
moeglicherweise kali ausgeliefert wird'''

import cx_Oracle 
import csv
import re

import logging
LOGGER = logging.getLogger(__name__)

with cx_Oracle.connect('string') as con:
    cur= con.cursor()
    bestand= con.cursor()
    bestand.prepare('INSERT INTO bestand (HAUPTABTEILUNG,BESTANDSNR,NEBENBESTAND) SELECT ?, ?, ? WHERE NOT EXISTS(SELECT 1 FROM bestand WHERE (HAUPTABTEILUNG,BESTANDSNR,NEBENBESTAND)=(?, ?, ?))')
    ae= con.cursor()
    ae.prepare('INSERT INTO AE (BESTAND_ID,UUID,ARCHIVNR,ARCHIVERGAENZUNGSBUCHSTABE) SELECT ID,?,?,? FROM bestand WHERE (HAUPTABTEILUNG,BESTANDSNR,NEBENBESTAND)=(?, ?, ?) AND NOT EXISTS(SELECT 1 FROM AE WHERE UUID= ?)')
    ae_import= con.cursor()
    ae_import.prepare('UPDATE AE_IMPORT SET AE_ID=(SELECT ID FROM AE WHERE UUID= :1 ) WHERE AE_UUID= :2 AND AE_ID IS NULL')
    for row in cur.execute('SELECT ORDNERNAME, AE_UUID FROM AE_IMPORT WHERE AE_ID IS NULL'):
        LOGGER.debug(row[0])
        m= re.match(r'^([A-Z])_(\d+)(?:_([A-Z]))?_(\d+)([a-z]?)$', row[0])
        hauptabt= m.group(1)
        bestnr= m.group(2)
        if m.group(3) != None:
            nbest= m.group(3)
        else:
            nbest= '@'
        archnr= m.group(4)
        if re.search(r'^[a-z]$',m.group(5),re.IGNORECASE):
            archbuch= m.group(5)
        else:
            archbuch= '@'
        LOGGER.debug('{} {}-{}/{}{}, {}'.format(hauptabt,bestnr,nbest,archnr,archbuch,row[1]))
        bestand.execute((hauptabt,int(bestnr),nbest,hauptabt,int(bestnr),nbest))
        ae.execute((row[1],archnr,archbuch,hauptabt,bestnr,nbest,row[1]))
        if cx_Oracle.sqlite_version < '3.33.0':
            ae_import.execute((row[1],row[1]))
    # UPDATE-FROM is supported beginning in SQLite version 3.33.0 (2020-08-14)
    # installiert ist select sqlite_version(); # 3.30.1
    if cx_Oracle.sqlite_version >= '3.33.0':
        cur.execute('UPDATE AE_IMPORT SET AE_ID=AE.ID FROM AE WHERE AE_UUID=AE.UUID AND AE_ID IS NULL')
    else:
        print("DB Version= {}".format(cx_Oracle.sqlite_version))
    con.commit()
                
