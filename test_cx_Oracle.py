#!/usr/bin/env python3
import unittest
import cx_Oracle 

class Test_cx_Oracle(unittest.TestCase):
    
    def test_makedsn(self):
        self.assertEqual(str,type(cx_Oracle.makedsn('localhost',5050,'S-1-5-21-7623811015-3361044348-030300820-1013')))
    
    def test_connect(self):
        self.assertEqual(cx_Oracle.Connection,type(cx_Oracle.connect('string')))
        
    def test_cursor(self):
        self.assertEqual(cx_Oracle.Cursor,type(cx_Oracle.connect('string').cursor()))
        
    def test_prepare(self):
        cur= cx_Oracle.connect('string').cursor()
        self.assertTrue(cur.prepare('SELECT * FROM bestand WHERE bestandsnr = :bnr'))
        self.assertEqual(cur.query, 'SELECT * FROM bestand WHERE bestandsnr = :bnr')
        
    def test_execute(self):
        con= cx_Oracle.connect('string')
        cur= con.cursor()
        cur.execute('create table if not EXISTS unit_test_table (a varchar[2], b int, c char[1], id INTEGER PRIMARY KEY AUTOINCREMENT)')
        cur.execute("INSERT INTO unit_test_table (a,b,c) VALUES ('B',323,'@'),('B',323,'@'),('B',905,'@')")
        self.assertEqual(cur.execute('SELECT ID FROM unit_test_table WHERE b = 323').fetchall(),[(1,),(2,)])
        cur.prepare('SELECT * FROM unit_test_table WHERE b = :bnr')
        self.assertEqual(cur.execute(bnr=905).fetchall(),[('B',905,'@',3)])
        cur.prepare('SELECT * FROM unit_test_table WHERE b >= :1')
        self.assertEqual(cur.execute([905]).fetchall(),[('B',905,'@',3)])
        cur.execute('DROP TABLE if EXISTS unit_test_table ')
        con.close()
        
    def test_ignoreKeyCase(self):
        with cx_Oracle.connect('string') as con:
            cur= con.cursor()
            cur.execute('create table if not EXISTS unit_test_table (a varchar[2], b int, c char[1], id INTEGER PRIMARY KEY AUTOINCREMENT)')
            cur.execute("INSERT INTO unit_test_table (a,b,c) VALUES ('B',323,'@'),('B',323,'@'),('B',905,'@')")
            self.assertEqual(cur.execute('SELECT ID FROM unit_test_table WHERE b = :UPPER',upper=323).fetchall(),[(1,),(2,)])
            cur.prepare('SELECT * FROM unit_test_table WHERE b = :lower')
            self.assertEqual(cur.execute(LoWER=905).fetchall(),[('B',905,'@',3)])
            cur.execute('DROP TABLE if EXISTS unit_test_table ')

    def test_sqlite_version(self):
        self.assertGreaterEqual(cx_Oracle.sqlite_version, '3.31.1', msg='sqlite3 < 3.31.1')
        self.assertGreaterEqual(cx_Oracle.sqlite_version, '3.33.0', msg='sqlite3 < 3.33.0')

    def tearDown(self):
        with cx_Oracle.connect('string') as con:
            con.execute('DROP TABLE if EXISTS unit_test_table ')

    
    
    
if __name__ == '__main__':
    unittest.main()
    
