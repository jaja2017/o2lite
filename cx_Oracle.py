#!/usr/bin/env python3
import re
import sqlite3
from sqlite3 import sqlite_version

import logging
LOGGER = logging.getLogger(__name__)

def makedsn(host, port, sid):
    LOGGER.debug("makedsn({}, {}, {})".format(host,port,sid))
    return "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=5500))(CONNECT_DATA=(SID=1)))"

def connect(*a,**k):
    conn= Connection("company.db")
    # conn= Connection("../../../Downloads/computer_cards.db")
    LOGGER.debug(type(conn))
    return conn

class Cursor(sqlite3.Cursor):
    
    def prepare(self, query):
        self.query= query
        # self.query= re.sub(r":\d+",'{}', query)
        # LOGGER.debug(self.query)
        return True

    def execute(self, liste='', *args, **kwargs):
        """ dies funktioniert out of the box mit sqlite3
        cur.execute("SELECT * FROM computer WHERE ram = :size ",{'size':2048})
        cur.execute("SELECT * FROM computer WHERE ram = ? ",[2048])
        cur.execute("SELECT * FROM computer WHERE ram >= :1 ",[2048])
        nicht jedoch mit (2048)
        aber
        cur.execute("SELECT * FROM computer WHERE ram >= :1 AND ram <= :2",(2024, 2048))
        """
        LOGGER.debug('def Cursor.execute({}, {}, {})'.format(liste, args, kwargs))
        if type(liste)==type("string") and re.search(r"(CREATE|INSERT|UPDATE|SELECT|DELETE|DROP)",liste,re.IGNORECASE):
            if kwargs.__len__() != 0:
                for k in kwargs.keys():
                    liste= re.sub(r":{}\b".format(k), ":{}".format(k), liste, flags=re.IGNORECASE)
                LOGGER.debug('super().execute({}, {})'.format(liste, kwargs))
                return super().execute(liste, kwargs)
            else:
                return super().execute(liste, *args)
        else:
            if kwargs.__len__() != 0:
                for k in kwargs.keys():
                    mquery= re.sub(r":{}\b".format(k), ":{}".format(k), self.query, flags=re.IGNORECASE)
                LOGGER.debug('super().execute({}, {})'.format(mquery, kwargs))
                return super().execute(mquery, kwargs)
            elif args.__len__() != 0:
                LOGGER.debug('super().execute({}, {})'.format(self.query, *args))
                return super().execute(self.query, *args)
            else:
                # LOGGER.debug('44: ',self.query, liste)
                return super().execute(self.query, liste)

    pass

class Connection(sqlite3.Connection):
    
    def cursor(self, factory=Cursor):
        cur= Cursor(self)
        LOGGER.debug(type(cur))
        return cur

    def prepare(self, query):
        cur= self.cursor()
        return cur.prepare(query)
    
    def execute(self, liste, *arr, **dictionary):
        """ 
        execute(sql[, parameters])
        This is a nonstandard shortcut that creates a cursor object by calling the cursor() method, calls the cursor's execute() method with the parameters given, and returns the cursor.
        """
        cur= self.cursor()
        return cur.execute(liste, *arr, **dictionary)

    pass



