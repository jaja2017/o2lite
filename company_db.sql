BEGIN TRANSACTION;
DROP TABLE IF EXISTS "ae";
CREATE TABLE IF NOT EXISTS "ae" (
	"BESTAND_ID"	INTEGER NOT NULL,
	"UUID"	char [36],
	"ARCHIVNR"	INTEGER,
	"ARCHIVERGAENZUNGSBUCHSTABE"	char [1]
);
DROP TABLE IF EXISTS "bestand";
CREATE TABLE IF NOT EXISTS "bestand" (
	"HAUPTABTEILUNG"	VARCHAR [2] NOT NULL,
	"BESTANDSNR"	INTEGER NOT NULL,
	"NEBENBESTAND"	INTEGER,
	"ID"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT
);
DROP TABLE IF EXISTS "ae_import";
CREATE TABLE IF NOT EXISTS "ae_import" (
	"ORDNERNAME"	varchar(24) NOT NULL,
	"AE_UUID"	char(36),
	PRIMARY KEY("AE_UUID")
);
DROP TABLE IF EXISTS "standort";
CREATE TABLE IF NOT EXISTS "standort" (
	"ortnr"	INTEGER,
	"kennung"	TEXT,
	"ortbez"	TEXT
);
INSERT INTO "ae" ("BESTAND_ID","UUID","ARCHIVNR","ARCHIVERGAENZUNGSBUCHSTABE") VALUES (1,'61f95e83-bff9-4b57-8b09-9d8920416055',1,'@'),
 (2,'fafe2724-f137-11ea-adc1-0242ac120000',0,'@'),
 (3,'fafe2968-f137-11ea-adc1-0242ac120002',100,'@');
INSERT INTO "bestand" ("HAUPTABTEILUNG","BESTANDSNR","NEBENBESTAND","ID") VALUES ('B',323,'@',1),
 ('B',323,'@',2),
 ('B',905,'@',3);
INSERT INTO "ae_import" ("ORDNERNAME","AE_UUID") VALUES ('B_323_0','fafe2724-f137-11ea-adc1-0242ac120000'),
 ('B_905_100','fafe2968-f137-11ea-adc1-0242ac120002'),
 ('C_110_1','fafe2968-f137-11ea-adc1-0242ac120001'),
 ('A_220_3','7399e551-dedd-4d68-a943-01508986251b'),
 ('B_747_M','c02e28d2-cf7f-4e39-8faa-49997b6defdd'),
 ('B_323_1','61f95e83-bff9-4b57-8b09-9d8920416055');
INSERT INTO "standort" ("ortnr","kennung","ortbez") VALUES (56075,'KO','Koblenz');
DROP TRIGGER IF EXISTS "AutoGenerateUUID_RELATION_1";
CREATE TRIGGER AutoGenerateUUID_RELATION_1
AFTER INSERT ON ae_import
FOR EACH ROW
WHEN (NEW.ae_uuid IS NULL)
BEGIN
   UPDATE ae_import SET ae_uuid = (select substr(u,1,8)||'-'||substr(u,9,4)||'-4'||substr(u,13,3)||
  '-'||v||substr(u,17,3)||'-'||substr(u,21,12) from (
    select lower(hex(randomblob(16))) as u, substr('89ab',abs(random()) % 4 + 1, 1) as v)) WHERE rowid = NEW.rowid;
END;
COMMIT;
