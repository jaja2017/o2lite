#!/usr/bin/env python3

import cx_Oracle 
import csv
import re

with cx_Oracle.connect('string') as con:
    cur= con.cursor()
    with open('konkordanz.csv') as csvfile:
        csvr= csv.reader(csvfile, delimiter=',')
        cur.prepare('INSERT INTO AE_IMPORT (AE_UUID,ORDNERNAME) SELECT :uuid, :ordner WHERE NOT EXISTS(SELECT 1 FROM AE_IMPORT WHERE (AE_UUID,ORDNERNAME)=(:uuid2, :ordner2))')
        for row in csvr:
            if re.search(r'^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$',row[0],re.IGNORECASE) and \
               re.search(r'^[A-Z]_\d+(_[A-Z])?_\d+[a-z]?$',row[1],re.IGNORECASE):
                cur.execute(uuid=row[0],ordner=row[1],uuid2=row[0],ordner2=row[1])
